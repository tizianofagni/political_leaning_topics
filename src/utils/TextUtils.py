def removeURL(text):
    import re
    return re.sub(r'http\S+', '', text, flags=re.IGNORECASE)


def returnFoundNames(text, knownAccounts):
    foundUsers = []
    for u in knownAccounts:
        if u in text:
            foundUsers.append(u)
    if len(foundUsers) == 0:
        return "@unk_user"
    else:
        return " ".join(foundUsers)


def cleanText(t, removeURLs, manipulateRTandAnswers, deleteRTtext):
    import re

    knownAccounts = ["@direzioneprc", "@maurizioacerbo", "@Piu_Europa", "@bendellavedova",
                     "@pdnetwork", "@nzingaretti", "@Mov5Stelle", "@luigidimaio",
                     "@forza_italia", "@berlusconi", "@LegaSalvini", "@matteosalvinimi",
                     "@FratellidItaIia", "@GiorgiaMeloni", "@CasaPoundItalia", "@distefanoTW"]
    knownAccounts = [t.lower() for t in knownAccounts]

    text = t
    text = text.lower()
    if removeURLs:
        text = removeURL(text)

    if deleteRTtext:
        noRTregex = r"^rt\s"
        match = re.search(noRTregex, text, re.IGNORECASE)
        if match:
            text = text[:match.start()]+""+text[match.end():]
    text = text.strip()

    saidByRegex = r"^(?:RT\s+)?(?:@\w+)(?:\s+@\w+)*\:"
    answerToRegex = r"^(?:RT\s+)?(?:@\w+)(?:\s+@\w+)*\s"

    if manipulateRTandAnswers:
        match = re.search(answerToRegex, text, re.IGNORECASE)
        if match:
            foundTxt = match[0]
            text = text.replace(foundTxt, "")
            users = returnFoundNames(foundTxt, knownAccounts)
            text = text + " -->> " + users

        match = re.search(saidByRegex, text, re.IGNORECASE)
        if match:
            foundTxt = match[0]
            text = text.replace(foundTxt, "")
            users = returnFoundNames(foundTxt, knownAccounts)
            text = users + " ==>> " + text

    return text


def optimizeContentForFinalTask(trainingData, removeURLs, manipulateRTandAnswers, deleteRTtext):
    ret = [cleanText(t, removeURLs, manipulateRTandAnswers, deleteRTtext) for t in trainingData]
    return ret



def generateExpData(df, tokenizer=None, textField="text", manipulateRTandAnswers = True, removeURLs=True, deleteRTtext = False):
    from tensorflow.keras.preprocessing.text import Tokenizer
    from tensorflow.keras.preprocessing.sequence import pad_sequences
    import numpy as np

    trainingData = df[textField].tolist()
    trainingData = optimizeContentForFinalTask(trainingData, removeURLs, manipulateRTandAnswers, deleteRTtext)

    if tokenizer is None:
        # Build characters dictionary using training data.
        tokenizer = Tokenizer(num_words=None, char_level=True, oov_token='UNK')
        tokenizer.fit_on_texts(trainingData)

    # Convert original texts into sequences of indices.
    train_sequences = tokenizer.texts_to_sequences(trainingData)

    # Pad data to sequences of max length 320.
    train_data = pad_sequences(train_sequences, maxlen=320, padding='post')
    train_data = np.array(train_data, dtype='float32')

    return train_data, tokenizer